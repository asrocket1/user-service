FROM golang:1.16 AS builder
WORKDIR /go/src/gitlab.com/service/
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app ./cmd/service

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/service/app .
CMD ["./app"]