package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

var root = &cobra.Command{
	Use:   "service",
	Short: "Golang service",
	RunE: func(cmd *cobra.Command, args []string) error {
		http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
			writer.WriteHeader(200)
			_, _ = writer.Write([]byte("Hello World!!"))
		})
		return http.ListenAndServe(":8080", nil)
	},
}

func main() {
	if err := root.Execute(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
